Feature: Clearing and unclearing transactions

Scenario: Transaction was just entered
  Given a new transaction
    | date       | payee | memo | amount |
    | 2014-01-01 | test  | test | 123.45 |
    Then the transaction should be marked as uncleared

Scenario Outline: Cleared/uncleared toggle
  Given a transaction is <status>
    When the user clicks the toggle button
    Then the transaction should be marked as <opposite>

Examples:
| status    | opposite  |
| uncleared | cleared   |
| cleared   | uncleared |
