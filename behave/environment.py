from selenium import webdriver
from time import sleep

URL = 'http://localhost:8000'

def before_feature(context, scenario):
    context.browser = webdriver.Firefox()
    context.url = URL
    context.browser.get(context.url + '/ledger/index.html')
    nuke = context.browser.find_element_by_name('nuke')
    nuke.submit()
    sleep(0.5)

def after_feature(context, feature):
    context.browser.get(context.url + '/ledger/index.html')
    nuke = context.browser.find_element_by_name('nuke')
    nuke.submit()
    sleep(0.5)
    context.browser.close()

def after_step(context, step):
    sleep(0.5)
