from behave import *
from selenium import webdriver

@given('a new transaction')
def step_impl(context):
    context.browser.get(context.url + '/ledger/index.html')
    for row in context.table:
        for elem in ('date', 'payee', 'memo', 'amount'):
            form = context.browser.find_element_by_name(elem)
            form.send_keys(str(row[elem]))
    submit = context.browser.find_element_by_name('submit_new')
    submit.submit()

@when('the user clicks the toggle button')
def step_impl(context):
    context.browser.get(context.url + '/ledger/index.html')
    button = context.browser.find_element_by_name('toggle_clear_unclear')
    button.submit()

@given('a transaction is cleared')
def step_impl(context):
    context.browser.get(context.url + '/ledger/index.html')
    assert 'value="C"' in context.browser.page_source

@given('a transaction is uncleared')
def step_impl(context):
    context.browser.get(context.url + '/ledger/index.html')
    assert 'value=" "' in context.browser.page_source

@then('the transaction should be marked as uncleared')
def step_impl(context):
    context.browser.get(context.url + '/ledger/index.html')
    assert 'value=" "' in context.browser.page_source
    assert 'value="C"' not in context.browser.page_source

@then('the transaction should be marked as cleared')
def step_impl(context):
    context.browser.get(context.url + '/ledger/index.html')
    assert 'value="C"' in context.browser.page_source
    assert 'value=" "' not in context.browser.page_source
