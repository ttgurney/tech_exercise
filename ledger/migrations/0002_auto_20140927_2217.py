# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ledger', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='amount',
            field=models.DecimalField(max_digits=8, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='memo',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='payee',
            field=models.CharField(default='', max_length=255),
        ),
    ]
