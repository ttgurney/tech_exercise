from django.db import models

class Transaction(models.Model):
    date = models.DateField()
    payee = models.CharField(max_length = 255, default='')
    memo = models.CharField(max_length = 255, default='')
    amount = models.DecimalField(max_digits = 8, decimal_places = 2)
    cleared = models.BooleanField(default = False)
    def __str__(self):
        return '{} : {:.2}'.format(self.id, self.amount)
