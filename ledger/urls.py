from django.conf.urls import patterns, url

from ledger import views

urlpatterns = patterns('',
    url(r'delete_all', views.delete_all, name='clear_all'),
    url(r'index.html', views.index, name='index'),
    url(r'toggle_cleared/([0-9][0-9]*)', views.toggle_cleared),
    url(r'^$', views.index, name='index')
)
