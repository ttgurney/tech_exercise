import datetime
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django import forms
from ledger.models import Transaction
from collections import namedtuple

class TransactionForm(forms.Form):
    date = forms.DateField(label='Date (yyyy-mm-dd)')
    payee = forms.CharField(max_length = 255)
    memo = forms.CharField(max_length = 255, required=False)
    amount = forms.DecimalField(max_digits = 8, decimal_places = 2)

def index(request):
    if request.method == 'POST':
        form = TransactionForm(request.POST)
        if form.is_valid():
            t = Transaction(**form.cleaned_data)
            t.save()
    form = TransactionForm()
    transactions = Transaction.objects.all().order_by('date')
    TString = namedtuple('tstring', ['date', 'payee', 'memo', 'debit', 'credit', 'cleared', 'id'])
    tstrings = [
        TString('{:04}-{:02}-{:02}'.format(t.date.year, t.date.month, t.date.day),
                t.payee,
                t.memo,
                '{:.2f}'.format(-t.amount) if t.amount < 0 else '',
                '{:.2f}'.format(t.amount) if t.amount >= 0 else '',
                t.cleared,
                t.id)
        for t in transactions
    ]
    clearedmoney = '{:.2f}'.format(sum(t.amount for t in transactions if t.cleared))
    unclearedmoney = '{:.2f}'.format(sum(t.amount for t in transactions if not t.cleared))
    totalmoney = '{:.2f}'.format(sum(t.amount for t in transactions))
    cleared_red = float(clearedmoney) < 0
    uncleared_red = float(unclearedmoney) < 0
    total_red = float(totalmoney) < 0
    context = {
                'transactions': tstrings,
                'form': form,
                'clearedmoney': clearedmoney,
                'cleared_red': cleared_red,
                'unclearedmoney': unclearedmoney,
                'uncleared_red': uncleared_red,
                'totalmoney': totalmoney,
                'total_red': total_red,
            }
    return render(request, 'ledger/index.html', context)

def delete_all(request):
    if request.method == 'POST':
        Transaction.objects.all().delete()
    return HttpResponseRedirect('/index.html')

def toggle_cleared(request, tid):
    if request.method == 'POST':
        print('in toggle_cleared ' + tid)
        t = Transaction.objects.get(id=tid)
        t.cleared = not t.cleared
        t.save()
    return HttpResponseRedirect('/index.html')
