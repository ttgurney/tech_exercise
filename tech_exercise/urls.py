from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tech_exercise.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ledger/', include('ledger.urls')),
    url(r'index.html', include('ledger.urls')),
    url(r'^$', include('ledger.urls'))
)
